package model.service;

import javax.swing.JOptionPane;


import controller.ClientController;
import model.dao.ClienteDao;
import model.dto.Cliente;
public class ClienteServ {

		private ClientController clienteController;
		public static boolean consultaCliente=false;
		public static boolean modificaCliente=false;
		/**
		 * @param clienteController the clienteController to set
		 */
		public void setClienteController(ClientController clienteController) {
			this.clienteController = clienteController;
		}
		/**
		 * @return the clienteController
		 */
		public ClientController getClienteController() {
			return clienteController;
		}
		
		
		//Metodo que valida los datos de Registro antes de pasar estos al DAO
		public void validarRegistro(Cliente miCliente) {
			ClienteDao miPersonaDao;
			//if (miCliente.getFecha())
				miPersonaDao = new ClienteDao();
				miPersonaDao.registrarCliente(miCliente);						
			/*}else {
				JOptionPane.showMessageDialog(null,"El documento de la persona debe ser mas de 3 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
				
			}*/
			
		}
		
		public Cliente validarConsulta(String codigoCliente) {
			ClienteDao miClienteDao;
			
			try {
				int codigo=Integer.parseInt(codigoCliente);	
				if (codigo > 0) {
					miClienteDao = new ClienteDao();
					consultaCliente=true;
					return miClienteDao.buscarCliente(codigo);						
				}else{
					JOptionPane.showMessageDialog(null,"El codigo del cliente no puede estar vacio.","Advertencia",JOptionPane.WARNING_MESSAGE);
					consultaCliente=false;
				}
				
			}catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
				consultaCliente=false;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
				consultaCliente=false;
			}
						
			return null;
		}
		
		//Metodo que valida los datos de Modificación antes de pasar estos al DAO
		public void validarModificacion(Cliente miCliente) {
			ClienteDao miClienteDao;
			if (miCliente.getNombre().length() > 0 && miCliente.getId() > 0) {
				miClienteDao = new ClienteDao();
				miClienteDao.modificarCliente(miCliente);	
				modificaCliente=true;
			}else{
				JOptionPane.showMessageDialog(null,"El nombre de la persona debe ser mayor a 5 digitos","Advertencia",JOptionPane.WARNING_MESSAGE);
				modificaCliente=false;
			}
		}
		
		//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
		public void validarEliminacion(String codigoCliente) {
			ClienteDao miClienteDao=new ClienteDao();
			try {
				int codigo=Integer.parseInt(codigoCliente);
				miClienteDao.eliminarCliente(codigo);
			}catch(NumberFormatException e) {
				JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
				consultaCliente=false;
			}
			
		}

		
		
}
